package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

func main() {
	addr := flag.String("url", "http://localhost:8080/health", "address url to call")
	dontFollowRedirect := flag.Bool("noredirect", false, "do not follow a redirect")
	flag.Parse()

	client := &http.Client{}
	if *dontFollowRedirect {
		client = &http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
	}

	req, err := http.NewRequest("GET", *addr, nil)
	if err != nil {
		log.Fatalf("Error creating Request: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("%v", err)
	}

	if 200 <= resp.StatusCode && resp.StatusCode < 300 {
		os.Exit(0)
	}

	log.Fatalf("Error checking health, got %v from %v", resp.Status, *addr)
}
