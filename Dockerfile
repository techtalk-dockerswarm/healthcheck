FROM golang:alpine AS build-env

# Add namespace here to resolve /vendor dependencies
ENV NAMESPACE gitlab.com/techtalk-dockerswarm/healthcheck
ENV MAINFILE healthcheck.go
WORKDIR /go/src/$NAMESPACE

ADD . ./
RUN CGO_ENABLED=0 GOOS=linux go build -v -ldflags '-w -s'  -a -installsuffix cgo -o /healthcheck $MAINFILE

FROM scratch
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-env /healthcheck /
